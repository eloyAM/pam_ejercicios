#include <assert.h>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#define MIN(a,b) (((a)<(b))?(a):(b))

// Compara la diferencia entre los valores de las dos matrices
double compare(double* ma, double* mb, const size_t size)
{
    double diff = 0.0;
    #pragma omp simd reduction(+:diff)
    for (size_t i = 0; i < size; i++) {
        diff += fabs(ma[i] - mb[i]);
    }
    return diff;
}

// Inicializa la matriz con valores aleatorios
void inicializar_matriz(double* ma, const size_t nfil, const size_t ncol)
{
    for (size_t i = 0; i < nfil; i++)
        for (size_t j = 0; j < ncol; j++)
            ma[i * ncol + j] = drand48();
}

// Rellena toda la matriz con el mismo valor
void inicializar_matriz_con_valor(double* ma, const size_t nfil, const size_t ncol, const double val)
{
    for (size_t i = 0; i < nfil; i++)
        #pragma omp simd
        for (size_t j = 0; j < ncol; j++)
            ma[i * ncol + j] = val;
}

// Version simple
void multiplicar_matrices_normal(double* ma, double* mb, const size_t nfil, const size_t ncol, double* res)
{
    for (size_t i = 0; i < nfil; i++) {
        for (size_t j = 0; j < ncol; j++) {
            double s = 0.0;
            #pragma omp simd reduction(+:s)
            for (size_t k = 0; k < ncol; k++) {
                s += ma[i * ncol + k] * mb[k * ncol + j];
            }
            res[i * ncol + j] = s;
        }
    }
}

// Version simple paralelizada
void multiplicar_matrices_normal_paralelo(double* ma, double* mb, const size_t nfil, const size_t ncol, double* res)
{
    #pragma omp parallel for schedule(static)
    for (size_t i = 0; i < nfil; i++) {
        for (size_t j = 0; j < ncol; j++) {
            double s = 0.0;
            #pragma omp simd reduction(+:s)
            for (size_t k = 0; k < ncol; k++) {
                s += ma[i * ncol + k] * mb[k * ncol + j];
            }
            res[i * ncol + j] = s;
        }
    }
}

// Version simple paralelizada con tareas
void multiplicar_matrices_normal_paralelo_tareas(double* ma, double* mb, const size_t nfil, const size_t ncol, double* res, const size_t nthreads)
{
    #pragma omp parallel num_threads(nthreads)
    #pragma omp single nowait
    for (size_t tarea = 1; tarea <= nthreads; tarea++) {
        #pragma omp task firstprivate(tarea) shared(ma, mb, res)
        for (size_t i = (tarea-1)*nfil/nthreads; i < (tarea)*nfil/nthreads; i++) {
            for (size_t j = 0; j < ncol; j++) {
                double s = 0.0;
                #pragma omp simd reduction(+:s)
                for (size_t k = 0; k < ncol; k++) {
                    s += ma[i * ncol + k] * mb[k * ncol + j];
                }
                res[i * ncol + j] = s;
            }
        }
    }
}

// Version con tiling (cache blocking)
void multiplicar_matrices_tiling(double* ma, double* mb, const size_t nfil, const size_t ncol, double* res, const size_t blsz)
{
    inicializar_matriz_con_valor(res, nfil, ncol, 0.0);
    size_t i, j, k, ii, jj, kk;
    for (ii = 0; ii < nfil; ii += blsz) {
        for (jj = 0; jj < ncol; jj += blsz) {
            for (kk = 0; kk < ncol; kk += blsz) {
                for (i = ii; i < MIN(ii + blsz, nfil); i++) {
                    for (j = jj; j < MIN(jj + blsz, ncol); j++) {
                        double sum = 0.0;
                        for (k = kk; k < MIN(kk + blsz, ncol); k++) {
                            sum += ma[i * ncol + k] * mb[k * ncol + j];
                        }
                        res[i * ncol + j] += sum;
                    }
                }
            }
        }
    }
}

// Version con tiling (cache blocking) y uso de hilos OMP
void multiplicar_matrices_tiling_paralelo(double* ma, double* mb, const size_t nfil, const size_t ncol, double* res, const size_t blsz)
{
    inicializar_matriz_con_valor(res, nfil, ncol, 0.0);
    size_t i, j, k, ii, jj, kk;
    #pragma omp parallel for private(i, j, k, ii, jj, kk) schedule(static)
    for (ii = 0; ii < nfil; ii += blsz) {
        for (jj = 0; jj < ncol; jj += blsz) {
            for (kk = 0; kk < ncol; kk += blsz) {
                for (i = ii; i < MIN(ii + blsz, nfil); i++) {
                    for (j = jj; j < MIN(jj + blsz, ncol); j++) {
                        double sum = 0.0;
                        for (k = kk; k < MIN(kk + blsz, ncol); k++) {
                            sum += ma[i * ncol + k] * mb[k * ncol + j];
                        }
                        res[i * ncol + j] += sum;
                    }
                }
            }
        }
    }
}

// Version con tiling (cache blocking) y uso de tareas OMP
void multiplicar_matrices_tiling_paralelo_tareas(double* ma, double* mb, const size_t nfil, const size_t ncol, double* res, const size_t blsz, const size_t nthreads)
{
    inicializar_matriz_con_valor(res, nfil, ncol, 0.0);
    const size_t nbloques = nfil / blsz;
    #pragma omp parallel num_threads(nthreads)
    #pragma omp single nowait
    {
        for (size_t tarea = 1; tarea < nthreads; tarea++) {
            #pragma omp task firstprivate(tarea) shared(ma, mb, res)
            for (size_t ii = ((tarea-1)*nbloques/nthreads)*blsz; ii < ((tarea)*nbloques/nthreads)*blsz; ii += blsz) {
                for (size_t jj = 0; jj < ncol; jj += blsz) {
                    for (size_t kk = 0; kk < ncol; kk += blsz) {
                        for (size_t i = ii; i < MIN(ii + blsz, nfil); i++) {
                            for (size_t j = jj; j < MIN(jj + blsz, ncol); j++) {
                                double sum = 0.0;
                                for (size_t k = kk; k < MIN(kk + blsz, ncol); k++) {
                                    sum += ma[i * ncol + k] * mb[k * ncol + j];
                                }
                                res[i * ncol + j] += sum;
                            }
                        }
                    }
                }
            }
        }
        // Ultima iteracion (y lo restante si nfil no es multiplo de blsz)
        #pragma omp task shared(ma, mb, res)
        for (size_t ii = ((nthreads-1)*nbloques/nthreads)*blsz; ii < nfil; ii += blsz) {
            for (size_t jj = 0; jj < ncol; jj += blsz) {
                for (size_t kk = 0; kk < ncol; kk += blsz) {
                    for (size_t i = ii; i < MIN(ii + blsz, nfil); i++) {
                        for (size_t j = jj; j < MIN(jj + blsz, ncol); j++) {
                            double sum = 0.0;
                            for (size_t k = kk; k < MIN(kk + blsz, ncol); k++) {
                                sum += ma[i * ncol + k] * mb[k * ncol + j];
                            }
                            res[i * ncol + j] += sum;
                        }
                    }
                }
            }
        }
    }
}

// Devuelve el instante de tiempo actual en segundos
double get_time(void)
{
    struct timeval t;
    gettimeofday(&t, (struct timezone*)0);
    return (((double)t.tv_sec) + ((double)t.tv_usec) * 1E-6);
}

void imprimir_resultado(const size_t matrix_dimension, const size_t blsz, const int nthreads, const char* version, const double time, const double speedup, const double diff)
{
    printf("%lu;%lu;%d;%s;%f;%f;%f\n", matrix_dimension, blsz, nthreads, version, time, speedup, diff);
    fflush(stdout);
}

// Realiza la multiplicacion de dos matrices con y sin tiling de forma secuencial y paralela
// Para el tamaño de matriz indicado, realiza varias ejecuciones variando el tamaño de bloque e hilos
// argumentos: <matrix_dimension> <max_threads>?
// Salida en formato csv
// MATRIX_SIZE;BLOCK_SIZE;THREADS;VERSION;TIME;SPEEDUP;DIFERENCIA
int main(int argc, char* argv[])
{
    if (argc == 1) {
        fprintf(stderr, "argumentos: <matrix_dimension> <max_threads>?\n");
        fprintf(stderr, "salida: MATRIX_SIZE;BLOCK_SIZE;THREADS;VERSION;TIME;SPEEDUP;DIFERENCIA\n");
        return EXIT_FAILURE;
    }

    const size_t nfil = atoi(argv[1]);
    const size_t blocksizes[] = { 64, 32, 16, 8 };
    const size_t blocksizes_len = sizeof(blocksizes) / sizeof(blocksizes[0]);
    const int maxthreads = (argc == 3) ? atoi(argv[2]) : omp_get_max_threads();

    const size_t ncol = nfil;
    const size_t msize = nfil * ncol;
    double ti, tnormal, tnormalparalelo, tnormalparaleloTareas, ttiling, ttilingparalelo, ttilingparaleloTareas; // Medidores del tiempo
    double *ma, *mb, *res, *expected; // Matrices
    // Reservar memoria
    ma = malloc(msize * sizeof(double));
    mb = malloc(msize * sizeof(double));
    res = malloc(msize * sizeof(double));
    expected = malloc(msize * sizeof(double));

    // Inicializar matrices
    srand48(getpid());
    inicializar_matriz(ma, nfil, ncol);
    inicializar_matriz(mb, nfil, ncol);

    // Multiplicacion normal: mc = ma x mb
    ti = get_time();
    multiplicar_matrices_normal(ma, mb, nfil, ncol, expected);
    tnormal = (get_time() - ti);
    imprimir_resultado(nfil, 1, 1, "normal", tnormal, 1.0, 0.0);

    // Para cada número de hilos desde el máximo disponible de dos en dos
    for (int nthreads = maxthreads; nthreads >= 2; nthreads -= 2) {
        // Multiplicacion normal paralela: mc = ma x mb
        inicializar_matriz_con_valor(res, nfil, ncol, -1.0);
        omp_set_num_threads(nthreads);
        ti = get_time();
        multiplicar_matrices_normal_paralelo(ma, mb, nfil, ncol, res);
        tnormalparalelo = (get_time() - ti);
        imprimir_resultado(nfil, 1, nthreads, "normal-paralelo", tnormalparalelo, tnormal / tnormalparalelo, compare(expected, res, msize));

        // Multiplicacion normal paralela con tareas: mc = ma x mb
        inicializar_matriz_con_valor(res, nfil, ncol, -1.0);
        ti = get_time();
        multiplicar_matrices_normal_paralelo_tareas(ma, mb, nfil, ncol, res, nthreads);
        tnormalparaleloTareas = (get_time() - ti);
        imprimir_resultado(nfil, 1, nthreads, "normal-paralelo-tasks", tnormalparaleloTareas, tnormal / tnormalparaleloTareas, compare(expected, res, msize));
    }

    // Para cada tamaño de bloque
    for (size_t bsizeindex = 0; bsizeindex < blocksizes_len; bsizeindex++) {
        const size_t blsz = blocksizes[bsizeindex];
        // Multiplicacion mejorada por bloques: mc = ma x mb
        inicializar_matriz_con_valor(res, nfil, ncol, -1.0);
        ti = get_time();
        multiplicar_matrices_tiling(ma, mb, nfil, ncol, res, blsz);
        ttiling = (get_time() - ti);
        imprimir_resultado(nfil, blsz, 1, "tiling", ttiling, tnormal / ttiling, compare(expected, res, msize));

        // Para cada número de hilos desde el máximo disponible de dos en dos
        for (int nthreads = maxthreads; nthreads >= 2; nthreads -= 2) {
            // Multiplicacion mejorada por bloques y paralela: mc = ma x mb
            inicializar_matriz_con_valor(res, nfil, ncol, -1.0);
            omp_set_num_threads(nthreads);
            ti = get_time();
            multiplicar_matrices_tiling_paralelo(ma, mb, nfil, ncol, res, blsz);
            ttilingparalelo = (get_time() - ti);
            imprimir_resultado(nfil, blsz, nthreads, "tiling-parallel-for", ttilingparalelo, tnormal / ttilingparalelo, compare(expected, res, msize));

            // Multiplicacion mejorada por bloques y paralela con tareas: mc = ma x mb
            inicializar_matriz_con_valor(res, nfil, ncol, -1.0);
            ti = get_time();
            multiplicar_matrices_tiling_paralelo_tareas(ma, mb, nfil, ncol, res, blsz, nthreads);
            ttilingparaleloTareas = (get_time() - ti);
            imprimir_resultado(nfil, blsz, nthreads, "tiling-parallel-tasks", ttilingparaleloTareas, tnormal / ttilingparaleloTareas, compare(expected, res, msize));
        }
    }

    // Liberar memoria y terminar el programa
    free(ma);
    free(mb);
    free(res);
    free(expected);
    return EXIT_SUCCESS;
}
