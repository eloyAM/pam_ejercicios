#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

// Inicializa la matriz con valores aleatorios
void inicializar_matriz(double* ma, const size_t nfil, const size_t ncol)
{
    for (size_t i = 0; i < nfil; i++)
        for (size_t j = 0; j < ncol; j++)
            ma[i * ncol + j] = drand48();
}

// Version simple de multiplicacion de matrices
void multiplicar_matrices_normal(double* ma, double* mb, const size_t nfil, const size_t ncol, double* res)
{
    for (size_t i = 0; i < nfil; i++) {
        for (size_t j = 0; j < ncol; j++) {
            double s = 0.0;
            #pragma omp simd reduction(+:s)
            for (size_t k = 0; k < ncol; k++) {
                s += ma[i * ncol + k] * mb[k * ncol + j];
            }
            res[i * ncol + j] = s;
        }
    }
}

// Devuelve el instante de tiempo actual en segundos
double get_time(void)
{
    struct timeval t;
    gettimeofday(&t, (struct timezone*)0);
    return (((double)t.tv_sec) + ((double)t.tv_usec) * 1E-6);
}

// Realiza la multiplicacion de dos matrices
// argumentos: <matrix_dimension>
// Salida en formato csv
// MATRIX_SIZE;BLOCK_SIZE;THREADS;VERSION;TIME;SPEEDUP;DIFERENCIA
int main(int argc, char* argv[])
{
    if (argc == 1) {
        fprintf(stderr, "argumentos: <matrix_dimension>\n");
        fprintf(stderr, "salida: MATRIX_SIZE;BLOCK_SIZE;THREADS;VERSION;TIME;SPEEDUP;DIFERENCIA\n");
        return EXIT_FAILURE;
    }

    const size_t nfil = atoi(argv[1]), ncol = nfil, msize = nfil * ncol;
    double ti, tnormal; // Medidores del tiempo
    double *ma, *mb, *res; // Matrices
    // Reservar memoria
    ma = malloc(msize * sizeof(double));
    mb = malloc(msize * sizeof(double));
    res = malloc(msize * sizeof(double));

    // Inicializar matrices
    srand48(getpid());
    inicializar_matriz(ma, nfil, ncol);
    inicializar_matriz(mb, nfil, ncol);

    // Multiplicacion normal: mc = ma x mb
    ti = get_time();
    multiplicar_matrices_normal(ma, mb, nfil, ncol, res);
    tnormal = (get_time() - ti);
    printf("%lu;%d;%d;%s;%f;%f;%f\n", nfil, 1, 1, "normal", tnormal, 1.0, 0.0);
    fflush(stdout);

    // Liberar memoria y terminar el programa
    free(ma);
    free(mb);
    free(res);
    return EXIT_SUCCESS;
}
