#include <stdio.h>

// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>

// Aux functions
#include <helper_cuda.h>

// Max elements on shared memory (kernel)
#define SHMEM 256
#define SHMEM_BYTES 4 * SHMEM

/**
 * CUDA Kernel Device code
 *
 * Calcula el producto escalar de dos vectores
 * for i=1..n
 *     escalar += v[i]*w[i]
 */
__global__ void
escalar(const float *v, const float *w, float *res, int n)
{
    extern __shared__ float sdata[];

    const unsigned int tidg = blockIdx.x * blockDim.x + threadIdx.x;

    sdata[threadIdx.x] = (tidg < n)? (v[tidg] * w[tidg]) : 0;
    __syncthreads();

    for (unsigned int s = blockDim.x/2; s > 0; s >>= 1) {
        if (threadIdx.x < s) {
            sdata[threadIdx.x] += sdata[threadIdx.x + s];
        }
        __syncthreads();
    }

    if (threadIdx.x == 0) {
        res[blockIdx.x] = sdata[0];
    }
}

// Realiza la reducción final de los resultado parciales
// de cada bloque de hilos calculado anteriormente por "escalar"
// Se ve limitado por el tamaño máximo de memoria compartida
__global__ void
reduction(const float *src, float *res, unsigned int n)
{
    extern __shared__ float sdata[];

    const unsigned int tidg = blockIdx.x * blockDim.x + threadIdx.x;

    sdata[threadIdx.x] = (tidg < n)? (src[tidg]) : 0;
    __syncthreads();

    for (unsigned int s = blockDim.x/2; s > 0; s >>= 1) {
        if (threadIdx.x < s) {
            sdata[threadIdx.x] += sdata[threadIdx.x + s];
        }
        __syncthreads();
    }

    if (threadIdx.x == 0) {
        res[blockIdx.x] = sdata[0];
    }
}

/**
 * Host main routine
 */
// USO:
// escalar -n=<tamañovectores> -bsx=<tambloqx> -gsx=<tamgridx>
int
main(int argc, char **argv)
{
    // Dimensiones de problema
    const int numElements = getCmdLineArgumentInt(argc, (const char**) argv, "n")? : 4096;
    const size_t nBytes = numElements * sizeof(float);
    printf("[Producto escalar de %d elementos]\n", numElements);
    // Dimensiones del kernel
    int inputBsx = getCmdLineArgumentInt(argc, (const char**) argv, "bsx");
    const int bsx = (inputBsx > 0 && inputBsx < SHMEM)? inputBsx : SHMEM;
    const size_t blockBytes = bsx * sizeof(float);
    const int gsx = getCmdLineArgumentInt(argc, (const char**) argv, "gsx")? : 16;
    const size_t gridBytes = gsx * sizeof(float);
    if (numElements > gsx * bsx) {
        fprintf(stderr, "DIMENSIONES INVALIDAS. Se debe generar al menos numElementos hilos\n");
        exit(EXIT_FAILURE);
    }
    // Eventos
    cudaEvent_t kernel_Start, kernel_Stop;
    checkCudaErrors(cudaEventCreate(&kernel_Start));
    checkCudaErrors(cudaEventCreate(&kernel_Stop));
    cudaEvent_t hostDev_Start, hostDev_Stop;
    checkCudaErrors(cudaEventCreate(&hostDev_Start));
    checkCudaErrors(cudaEventCreate(&hostDev_Stop));
    cudaEvent_t devHost_Start, devHost_Stop;
    checkCudaErrors(cudaEventCreate(&devHost_Start));
    checkCudaErrors(cudaEventCreate(&devHost_Stop));

    // HOST vectors
    float *h_V = NULL, *h_W = NULL, *h_Res = NULL;
    h_V = (float*) malloc(nBytes);
    h_W = (float*) malloc(nBytes);
    h_Res = (float*) malloc(gridBytes);
    // Verify that allocations succeeded
    if (h_V == NULL || h_W == NULL || h_Res == NULL) {
        fprintf(stderr, "Failed to allocate host vectors!\n");
        exit(EXIT_FAILURE);
    }
    // Initialize the host input vectors
    for (int i = 0; i < numElements; ++i) {
        h_V[i] = 1;
        h_W[i] = 1;
    }

    // DEVICE vectors
    float *d_V = NULL, *d_W = NULL, *d_Res = NULL;
    checkCudaErrors(cudaMalloc((void** )&d_V, nBytes));
    checkCudaErrors(cudaMalloc((void** )&d_W, nBytes));
    checkCudaErrors(cudaMalloc((void** )&d_Res, gridBytes));
    // Copy the host input vectors in host memory
    // to the device input vectors in device memory
    printf("Copy input data from the host memory to the CUDA device\n");
    checkCudaErrors(cudaEventRecord(hostDev_Start));
    checkCudaErrors(cudaMemcpy(d_V, h_V, nBytes, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_W, h_W, nBytes, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaEventRecord(hostDev_Stop));

    /*------------------KERNEL------------------------*/
    // Launch the Vector Add CUDA Kernel
    printf("CUDA kernel launch with %d blocks of %d threads\n", gsx, bsx);
    checkCudaErrors(cudaEventRecord(kernel_Start));
    escalar<<<gsx, bsx, blockBytes>>>(d_V, d_W, d_Res, numElements);
    checkCudaErrors(cudaGetLastError());
    reduction<<<1, gsx, gridBytes>>>(d_Res, d_Res, gsx);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaEventRecord(kernel_Stop));

    // Copy the device result vector in device memory to the host result vector in host memory.
    printf("Copy output data from the CUDA device to the host memory\n");
    checkCudaErrors(cudaEventRecord(devHost_Start));
    checkCudaErrors(cudaMemcpy(h_Res, d_Res, gridBytes, cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaEventRecord(devHost_Stop));

    // Timing
    checkCudaErrors(cudaEventSynchronize(kernel_Stop));
    float milliseconds = -1.0;
    checkCudaErrors(cudaEventElapsedTime(&milliseconds, kernel_Start, kernel_Stop));
    printf("Kernel execution time: %f\n", milliseconds);
    checkCudaErrors(cudaEventSynchronize(hostDev_Stop));
    checkCudaErrors(cudaEventElapsedTime(&milliseconds, hostDev_Start, hostDev_Stop));
    printf("Host to device copy memory time: %f\n", milliseconds);
    checkCudaErrors(cudaEventSynchronize(devHost_Stop));
    checkCudaErrors(cudaEventElapsedTime(&milliseconds, devHost_Start, devHost_Stop));
    printf("Device to host copy memory time: %f\n", milliseconds);

    // Verify that the result is correct
    float expectedResult = 0.0;
    for (int i = 0; i < numElements; ++i) {
        expectedResult += h_V[i] * h_W[i];
    }
    if (fabs(expectedResult - h_Res[0]) > 1e-5) {
        fprintf(stderr, "Test FAILED Got %f Expected %f\n", h_Res[0], expectedResult);
        exit(EXIT_FAILURE);
    }
    printf("Test PASSED\n");

    // Free device global memory
    checkCudaErrors(cudaFree(d_V));
    checkCudaErrors(cudaFree(d_W));
    checkCudaErrors(cudaFree(d_Res));

    // Free host memory
    free(h_V);
    free(h_W);
    free(h_Res);

    // Destroy events
    checkCudaErrors(cudaEventDestroy(kernel_Start));
    checkCudaErrors(cudaEventDestroy(kernel_Stop));
    checkCudaErrors(cudaEventDestroy(hostDev_Start));
    checkCudaErrors(cudaEventDestroy(hostDev_Stop));
    checkCudaErrors(cudaEventDestroy(devHost_Start));
    checkCudaErrors(cudaEventDestroy(devHost_Stop));

    // Reset the device and exit
    // cudaDeviceReset causes the driver to clean up all state. While
    // not mandatory in normal operation, it is good practice.  It is also
    // needed to ensure correct operation when the application is being
    // profiled. Calling cudaDeviceReset causes all profile data to be
    // flushed before the application exits
    checkCudaErrors(cudaDeviceReset());

    printf("Done\n");
    return 0;
}

