/**
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/**
 * Vector addition: C = A + B.
 *
 * This sample is a very basic sample that implements element by element
 * vector addition. It is the same as the sample illustrating Chapter 2
 * of the programming guide with some additions like error checking.
 */

#include <stdio.h>

// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>

// Aux functions
#include <helper_cuda.h>

/**
 * CUDA Kernel Device code
 *
 * Computes the vector addition of A and B into C. The 3 vectors have the same
 * number of elements numElements.
 */
__global__ void
vectorAdd(const float *A, const float *B, float *suma, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;

    if (i < numElements)
    {
        suma[i] = A[i] + B[i];
    }
}

/**
 * Host main routine
 */
// USO:
// vectorAdd -ns=<numero_streams> -n=<tamaño_vector> -b=<tamaño_bloques>
int
main(int argc, char **argv)
{
    // Dimensiones de problema
    const int numElements = getCmdLineArgumentInt(argc, (const char**) argv, "n")? : 4096;
    const size_t size = numElements * sizeof(float);
    printf("[Vector addition of %d elements]\n", numElements);
    // Dimensiones del kernel
    const int threadsPerBlock = getCmdLineArgumentInt(argc, (const char**) argv, "b")? : 256;
    const int blocksPerGrid = (numElements + threadsPerBlock - 1) / threadsPerBlock;

    // Streams
    const int numStreams = getCmdLineArgumentInt(argc, (const char**) argv, "ns")? : 4;
    const int streamElements = numElements / numStreams;
    const int streamByes = size / numStreams;
    cudaStream_t streams[numStreams];
    for (int i = 0; i < numStreams; ++i) {
        checkCudaErrors(cudaStreamCreate(&streams[i]));
    }
    printf("%d streams\n", numStreams);

    // Si el num elementos no es division exacta con el num streams
    // uno de ellos debe operar tambien los elementos restantes
//    const int nDivisibleStreams = (numElements % numStreams == 0);
    const int elementosRestantes = streamElements + (numElements % numStreams);
    const size_t bytesRestantes = elementosRestantes * sizeof(float);


    // Eventos
    cudaEvent_t start, stop;
    checkCudaErrors(cudaEventCreate(&start));
    checkCudaErrors(cudaEventCreate(&stop));


    // HOST vectors
    float *h_A, *h_B, *h_C;
    checkCudaErrors(cudaMallocHost((void **)&h_A, size));
    checkCudaErrors(cudaMallocHost((void **)&h_B, size));
    checkCudaErrors(cudaMallocHost((void **)&h_C, size));

    // Initialize the host input vectors
    for (int i = 0; i < numElements; ++i)
    {
        h_A[i] = rand()/(float)RAND_MAX;
        h_B[i] = rand()/(float)RAND_MAX;
    }

    // DEVICE vectors
    float *d_A = NULL, *d_B = NULL, *d_C = NULL;
    checkCudaErrors(cudaMalloc((void **)&d_A, size));
    checkCudaErrors(cudaMalloc((void **)&d_B, size));
    checkCudaErrors(cudaMalloc((void **)&d_C, size));

    // Copy the host input vectors A and B in host memory to the device input vectors in
    // device memory
    printf("Copy input data from the host memory to the CUDA device\n");
    checkCudaErrors(cudaEventRecord(start));
    for (int i = 0; i < numStreams; ++i)
    {
        const int off = i * streamElements;
        const size_t count = (i != numStreams - 1)? streamByes : bytesRestantes;
        checkCudaErrors(
                cudaMemcpyAsync(&d_A[off], &h_A[off], count,
                        cudaMemcpyHostToDevice, streams[i]));
        checkCudaErrors(
                cudaMemcpyAsync(&d_B[off], &h_B[off], count,
                        cudaMemcpyHostToDevice, streams[i]));
    }

    /*------------------KERNEL------------------------*/
    // Launch the Vector Add CUDA Kernel
    printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, threadsPerBlock);
    for (int i = 0; i < numStreams; ++i)
    {
        const int off = i * streamElements;
        const int kernelElements = (i != numStreams - 1)? streamElements : elementosRestantes;
        vectorAdd<<<blocksPerGrid, threadsPerBlock, 0, streams[i]>>>(&d_A[off], &d_B[off], &d_C[off], kernelElements);
        checkCudaErrors(cudaGetLastError());
    }
    /*-----------------------------------------------*/

    // Copy the device result vector in device memory to the host result vector in host memory.
    printf("Copy output data from the CUDA device to the host memory\n");
    for (int i = 0; i < numStreams; ++i)
    {
        const int off = i * streamElements;
        const size_t count = (i != numStreams - 1)? streamByes : bytesRestantes;
        checkCudaErrors(
                cudaMemcpyAsync(&h_C[off], &d_C[off], count,
                        cudaMemcpyDeviceToHost, streams[i]));
    }

    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));
    float elapsedTime = -1;
    checkCudaErrors(cudaEventElapsedTime(&elapsedTime, start, stop));
    printf("Memory and execution time: %f ms\n", elapsedTime);

    // Verify that the result vector is correct
    for (int i = 0; i < numElements; ++i)
    {
        if (fabs(h_A[i] + h_B[i] - h_C[i]) > 1e-5)
        {
            fprintf(stderr, "Result verification of sum failed at element %d"
                    " -> Got: %f Expected: %f\n", i, h_C[i], h_A[i] + h_B[i]);
            exit(EXIT_FAILURE);
        }
    }

    printf("Test PASSED\n");

    // Free device global memory
    checkCudaErrors(cudaFree(d_A));
    checkCudaErrors(cudaFree(d_B));
    checkCudaErrors(cudaFree(d_C));

    // Free host memory
    checkCudaErrors(cudaFreeHost(h_A));
    checkCudaErrors(cudaFreeHost(h_B));
    checkCudaErrors(cudaFreeHost(h_C));

    // Destroy streams
    for (int i = 0; i < numStreams; ++i) {
        checkCudaErrors(cudaStreamDestroy(streams[i]));
    }

    // Destroy events
    checkCudaErrors(cudaEventDestroy(start));
    checkCudaErrors(cudaEventDestroy(stop));

    // Reset the device and exit
    // cudaDeviceReset causes the driver to clean up all state. While
    // not mandatory in normal operation, it is good practice.  It is also
    // needed to ensure correct operation when the application is being
    // profiled. Calling cudaDeviceReset causes all profile data to be
    // flushed before the application exits
    checkCudaErrors(cudaDeviceReset());

    printf("Done\n");
    return 0;
}

