/**
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/**
 * Vector addition: C = A + B.
 *
 * This sample is a very basic sample that implements element by element
 * vector addition. It is the same as the sample illustrating Chapter 2
 * of the programming guide with some additions like error checking.
 */

#include <stdio.h>

// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>

// Aux functions
#include <helper_cuda.h>

/**
 * CUDA Kernel Device code
 *
 * Computes the vector addition of A and B into C. The 3 vectors have the same
 * number of elements numElements.
 */
__global__ void
vectorAdd(const float *A, const float *B, float *suma, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
 
    if (i < numElements)
    {
        suma[i] = A[i] + B[i];
    }
}

/**
 * Host main routine
 */
// USO:
// vectorAdd -n=<tamaño_vector> -b=<tamaño_bloques>
int
main(int argc, char** argv)
{
    // Dimensiones del problema
    const int numElements = getCmdLineArgumentInt(argc, (const char**) argv, "n")? : 4096;
    const size_t size = numElements * sizeof(float);
    printf("[Vector addition of %d elements]\n", numElements);
    // Dimensiones del kernel
    const int threadsPerBlock = getCmdLineArgumentInt(argc, (const char**) argv, "b")? : 256;
    const int blocksPerGrid = (numElements + threadsPerBlock - 1) / threadsPerBlock;
 
 
    // DEVICE vectors
    float *A = NULL, *B = NULL, *C = NULL;
    checkCudaErrors(cudaMallocManaged((void **)&A, size));
    checkCudaErrors(cudaMallocManaged((void **)&B, size));
    checkCudaErrors(cudaMallocManaged((void **)&C, size));
 
    // Initialize the host input vectors
    for (int i = 0; i < numElements; ++i)
    {
        A[i] = rand()/(float)RAND_MAX;
        B[i] = rand()/(float)RAND_MAX;
    }

    /*------------------KERNEL------------------------*/
    // Launch the Vector Add CUDA Kernel
    printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, threadsPerBlock);
    vectorAdd<<<blocksPerGrid, threadsPerBlock>>>(A, B, C, numElements);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaDeviceSynchronize());
    /*-----------------------------------------------*/
 
    // Verify that the result vector is correct
    for (int i = 0; i < numElements; ++i)
    {
        if (fabs(A[i] + B[i] - C[i]) > 1e-5)
        {
            fprintf(stderr, "Result verification of sum failed at element %d"
                    " -> Got: %f Expected: %f\n", i, C[i], A[i] + B[i]);
            exit(EXIT_FAILURE);
        }
    }
 
    printf("Test PASSED\n");
 
    // Free device global memory
    checkCudaErrors(cudaFree(A));
    checkCudaErrors(cudaFree(B));
    checkCudaErrors(cudaFree(C));
 
    // Reset the device and exit
    // cudaDeviceReset causes the driver to clean up all state. While
    // not mandatory in normal operation, it is good practice.  It is also
    // needed to ensure correct operation when the application is being
    // profiled. Calling cudaDeviceReset causes all profile data to be
    // flushed before the application exits
    checkCudaErrors(cudaDeviceReset());
 
    printf("Done\n");
    return 0;
}
 
 
