#if defined (_OPENMP)
	#include <omp.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct Tree {
	int value;
	struct Tree *izq;
	struct Tree *der;
};

typedef struct Tree AB;

void add(AB **raiz, int e)
{
	AB *t;
	
	if (*raiz == NULL) {
		t = (AB *) malloc(sizeof(AB));
		t->value = e;
		t->izq = t->der = NULL;
		*raiz = t;
	}
	else if(e < (*raiz)->value) {
		add(&(*raiz)->izq, e);
	}
	else if(e > (*raiz)->value) {
		add(&(*raiz)->der, e);
	}
}

void process(int value) {
	int tid = 0;
	#if defined (_OPENMP)
		tid = omp_get_thread_num();
	#endif
	printf("Processing Node with Value: %d from Thread: %d\n", value, tid);
}

void preorden(AB *ab)
{
	#pragma omp taskwait
	process(ab->value);
	
	if (ab->izq) {
		#pragma omp task
		preorden(ab->izq);
	}
	
	if (ab->der) {
		#pragma omp task
		preorden(ab->der);
	}
}

void postorden(AB *ab)
{
	if (ab->izq) {
		#pragma omp task
		postorden(ab->izq);
	}
	
	if (ab->der) {
		#pragma omp task
		postorden(ab->der);
	}

	#pragma omp taskwait
	process(ab->value);
}


int main(int argc, char *argv[])
{
	int nt, n, max = 1;
	AB *btree;
	
	srand(getpid());
	
	#if defined (_OPENMP)
		max = omp_get_max_threads();
	#endif
	
	if(argc < 3) {
		fprintf(stderr, "Usage: %s <n_elem> <omp_threads>\n", argv[0]);
		return(EXIT_FAILURE);
	}
	n  = atoi(argv[1]);
	nt = (max > 1) ? atoi(argv[2]) : max;
	
	if((n < 0) || (nt < 1)) {
		fprintf(stderr, "Invalid Value for Parameters.\n");
		fprintf(stderr, "Exiting...\n");
		return(EXIT_FAILURE);
	}
	
	printf("\n");
	printf("MAX_OMP_THREADS: %d\n", max);
	printf("OMP_THREADS: %d\n", nt);
	printf("\n");
	
	btree = NULL;
	for (int i = 0; i < n; i++) {
		// add(&btree, (rand() % 1000) + 1);
		add(&btree, i);
	}

	printf("Preorden\n");
	#if defined (_OPENMP)
		omp_set_num_threads(nt);
	#endif
	#pragma omp parallel
	#pragma omp single nowait
	preorden(btree);

	printf("\nPostorden\n");
	#if defined (_OPENMP)
		omp_set_num_threads(nt);
	#endif
	#pragma omp parallel
	#pragma omp single nowait
	postorden(btree);
	
	printf("\n");	
	return(EXIT_SUCCESS);
}
