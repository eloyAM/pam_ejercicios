#if defined (_OPENMP)
	#include <omp.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#define SCHED static
#define CHUNK 2

int main(int argc, char *argv[])
{
	int i;
	int tid = 0, nt, max = 1;
	char str[12] = "Hello World";
	
	#if defined (_OPENMP)
		max = omp_get_max_threads();
	#endif
	
	if(argc < 2) {
		fprintf(stderr, "Usage: %s <omp_threads>\n", argv[0]);
		return(EXIT_FAILURE);
	}
	nt = (max > 1) ? atoi(argv[1]) : max;
	
	if(nt < 1) {
		fprintf(stderr, "Invalid Value for OMP_THREADS...\n");
		return(EXIT_FAILURE);
	}
	
	printf("\n");
	printf("MAX_OMP_THREADS: %d\n", max);
	printf("OMP_THREADS: %d\n", nt);
	printf("\n");
	
	#pragma omp parallel shared(str) firstprivate(nt,max) private(tid) num_threads(nt)
	{
		#if defined (_OPENMP)
			tid = omp_get_thread_num();
		#endif
		printf("%s from Thread %d out of %d\n", str, tid, nt);
		
		#pragma omp for private(i) schedule(SCHED,CHUNK)
		for(i=0; i<(nt*max); i++) {
			printf("Thread %d: Iteracion %d\n", tid, i);
		}
	}
	
	printf("\n");
	return(EXIT_SUCCESS);
}
