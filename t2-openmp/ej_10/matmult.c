#if defined (_OPENMP)
	#include <omp.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>
#include <unistd.h>

void init_data(double *d, int size)
{
	int i;
	srand(getpid());
	
	for(i=0; i<size; i++) {
		d[i] = (double) rand()/RAND_MAX;
	}
}

double compare(double *m1, double *m2, int size)
{
	int i;
	double diff = 0.0;
	
	for(i=0; i<size; i++) {
		diff += fabs(m1[i] - m2[i]);
	}
	return diff;
}

void mm_sec(double *a, int fa, int ca, int lda, \
			double *b, int fb, int cb, int ldb, \
			double *c, int fc, int cc, int ldc)
{
	for(int i=0; i<fa; i++) {
		for(int j=0; j<cb; j++) {
			double s = 0.0;
			for(int k=0; k<ca; k++) {
				s += a[i*lda + k] * b[k*ldb + j];
			}
			c[i*ldc + j] = s;
		}
	}
}

void procesar(int i,
			  double *a, int ca, int lda,
			  double *b, int cb, int ldb,
			  double *c, int ldc)
{
	for(int j=0; j<cb; j++) {
		double s = 0.0;
		for(int k=0; k<ca; k++) {
			s += a[i*lda + k] * b[k*ldb + j];
		}
		c[i*ldc + j] = s;
	}
}

/* 10.e Vectorizacion */
//__attribute__((optimize("no-tree-vectorize")))
void mm_par(double *a, int fa, int ca, int lda, \
			double *b, int fb, int cb, int ldb, \
			double *c, int fc, int cc, int ldc)
{
	#pragma omp parallel for
	for(int i=0; i<fa; i++) {
		for(int j=0; j<cb; j++) {
			double s = 0.0;
			#pragma omp simd reduction(+:s)
			for(int k=0; k<ca; k++) {
				s += a[i*lda + k] * b[k*ldb + j];
			}
			c[i*ldc + j] = s;
		}
	}
}

double get_time( void ) {
    struct timeval t;
    gettimeofday (&t, (struct timezone *)0);
    return (((double) t.tv_sec) + ((double) t.tv_usec)*1E-6);
}

int main(int argc, char *argv[])
{
	int max = 1, nt;
	double ti, ts, tp;
	double *A, *B, *C, *C2;
	
	#if defined (_OPENMP)
		max = omp_get_max_threads();
	#endif
	
	if(argc < 5) {
		fprintf(stderr, "Usage: %s <rows_mA> <cols_mA> <cols_mB> <omp_threads>\n", argv[0]);
		return(-1);
	}
	int fa = atoi(argv[1]);
	int ca = atoi(argv[2]);
	int cb = atoi(argv[3]);
	assert((fa > 0) && (ca > 0) && (cb > 0));
	
	nt = (max > 1) ? atoi(argv[4]) : max;
	assert(nt > 0);
	
	int size_mA = fa*ca;
	int size_mB = ca*cb;
	int size_mC = fa*cb;
	
	A = (double *) malloc(sizeof(double)*size_mA);
	B = (double *) malloc(sizeof(double)*size_mB);
	C = calloc(size_mC, sizeof(double));
	
	// Matrix to Store Parallel Solution
	C2 = calloc(size_mC, sizeof(double));
	
	/* Initialize Matrices */
	init_data(A, size_mA);
	init_data(B, size_mB);
	
	ti = get_time();
	mm_sec(A, fa, ca, ca, B, ca, cb, cb, C, fa, cb, cb);	// version secuencial
	ts = (get_time() - ti);
	
	printf("\n");
	printf("Tiempo Secuencial: %.2lf segundos\n", ts);
	
	#if defined (_OPENMP)
		omp_set_num_threads(nt);
	#endif
	
	ti = get_time();
	mm_par(A, fa, ca, ca, B, ca, cb, cb, C2, fa, cb, cb);	// version paralela
	tp = (get_time() - ti);
	printf("Tiempo Paralelo:   %.2lf segundos\n", tp);
	
	printf("\n");
	printf("Diferencia: %.2lf\n", compare(C, C2, size_mC));
	printf("Speed-Up =  %.2lf\n", ts/tp);
	printf("\n");
	
	/* Free Allocated Memory */
	free(A); free(B); free(C); free(C2);
	
	return(0);
}
