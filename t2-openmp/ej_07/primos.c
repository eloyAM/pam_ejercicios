#if defined (_OPENMP)
	#include <omp.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#define N_PRIMOS 10000

// Comparar enteros para qsort
int cmpfunc(const void *a, const void *b) {
	return (*(int *)a - *(int *)b);
}

void print_data(int *v)
{
	int i;
	for(i=0; i<N_PRIMOS; i++) {
		if(v[i] != 0) {
			printf("%d ", v[i]);
		}
	}
	printf("\n");
}

int primos_sec(int *v, int n)
{
	int acum = 0;

	for (int i = 2; i <= n; i++) {
		int primo = 1;
		for (int j = 2; j < i; j++) {
			if((i % j) == 0) {
				primo = 0;
				break;
			}
		}
		if(primo) {
			v[acum] = i;
			acum++;
			if(acum == N_PRIMOS) { break; }
		}
	}
	return acum;
}

int primos_par(int *v, int n) {
	// Array de flags (booleanos con char para ahorrar memoria)
	char *flag = calloc(n, sizeof(char));
	#pragma omp parallel for
	for (int i = 2; i <= n; i++) {
		char is_primo = '1';
		for (int j = 2; j < i; j++) {
			if ((i % j) == 0) {
				is_primo = '0';
				break;
			}
		}
		flag[i] = is_primo;
	}
	
	int acum = 0;
	for (int i = 0; i < n && acum < N_PRIMOS; i++)
		if (flag[i] == '1')
			v[acum++] = i;
		
	free(flag);
	return acum;
}

double get_time( void ) {
    struct timeval t;
    gettimeofday (&t, (struct timezone *)0);
    return (((double) t.tv_sec) + ((double) t.tv_usec)*1E-6);
}


int main(int argc, char *argv[])
{
	int n, nt, max = 1;

	#if defined (_OPENMP)
		max = omp_get_max_threads();
	#endif
	
	if(argc < 3) {
		fprintf(stderr, "Usage: %s <n> <omp_threads>\n", argv[0]);
		return(EXIT_FAILURE);
	}
	n  = atoi(argv[1]);
	nt = (max > 1) ? atoi(argv[2]) : max;
	
	if((n < 0) || (nt < 1)) {
		fprintf(stderr, "Invalid Value for Parameters.\n");
		fprintf(stderr, "Exiting...\n");
		return(EXIT_FAILURE);
	}
	
	int *v1 = calloc(N_PRIMOS, sizeof(int));
	int *v2 = calloc(N_PRIMOS, sizeof(int));
	double ti, ts, tp;

	#if defined (_OPENMP)
		omp_set_num_threads(nt);
	#endif
	
	printf("\n");
	ti = get_time();
	int acum1 = primos_sec(v1, n);	// version secuencial
	ts = get_time() - ti;
	printf("Tiempo Secuencial: %.4lf segundos\n", ts);
	// print_data(v1);
	
	printf("\n");
	#if defined (_OPENMP)	
		omp_set_num_threads(nt);
	#endif
	ti = get_time();
	int acum2 = primos_par(v2, n);	// version paralela
	tp = get_time() - ti;
	printf("Tiempo Paralelo: %.4lf segundos\n", tp);
	// print_data(v2);

	printf("Speed-Up =  %.2lf\n", ts/tp);

	int exit_status = 0;
	if (acum1 != acum2) {
		fprintf(stderr, "Los resultados difieren en el num de primos: %d != %d\n", acum1, acum2);
		exit_status = 1;
	}
	for (int i = 0; i < acum1; i++) {
		if (v1[i] != v2[i]) {
			fprintf(stderr, "Los resultados difieren en los valores calculados: %d != %d\n", v1[i], v2[i]);
			exit_status = 1;
			break;
		}
	}
	free(v1);
	free(v2);
	printf("\n");
	return(exit_status);
}
