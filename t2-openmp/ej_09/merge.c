#if defined (_OPENMP)
	#include <omp.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

void init_data(double *v, int n)
{
	int i;
	srand(getpid());
	
	for(i=0; i<n; i++) {
		v[i] = (double) rand()/RAND_MAX;
	}
}

int compare(double *v1, double *v2, int n)
{
	int i;
	int diff = 0;
	
	for(i=0; i<n; i++) {
		if(v1[i] != v2[i]) {
			diff = 1;
			break;
		}
	}
	return diff;
}

void sort(double *v, int n)
{
	int i, j;
	double temp;
	
	for(i=0; i<n; i++) {
		for(j=i+1; j<n; j++) {
			if (v[i] > v[j]) {
				temp = v[i];
				v[i] = v[j];
				v[j] = temp;
			}
		}
	}
}

void merge(double *v, int izq, int med, int der)
{
	int i, j, k;
	double *temp = (double *) malloc(sizeof(double)*(der - izq));
	
	k = 0;
	i = izq;
	j = med;
	while((i < med) && (j < der))
	{
		if(v[i] < v[j]) {
			temp[k] = v[i];
			i++;
		}
		else {
			temp[k] = v[j];
			j++;
		}
		k++;
	}
	
	for(; i<med; i++) {
		temp[k] = v[i];
		k++;
	}
	
	for(; j<der; j++) {
		temp[k] = v[j];
		k++;
	}
	
	for(i=0; i<der-izq; i++) {
		v[i+izq] = temp[i];
	}
	free(temp);
}

void merge_sec(double *v, int n)
{
	if(n < 2) { return; }
	
	merge_sec(v, n/2);
	merge_sec(&v[n/2], n-n/2);
	
	merge(v, 0, n/2, n);
}

void merge_par(double *v, int n, int nthreads)
{
	if(n < 2) { return; }
	if (nthreads == 1)
		merge_sec(v, n);
	else
	{
		#pragma omp task
		merge_par(v, n/2, nthreads/2);
		#pragma omp task
		merge_par(&v[n/2], n-n/2, nthreads/2);
		
		#pragma omp taskwait
		merge(v, 0, n/2, n);
	}
}

double get_time( void ) {
    struct timeval t;
    gettimeofday (&t, (struct timezone *)0);
    return (((double) t.tv_sec) + ((double) t.tv_usec)*1E-6);
}

int main(int argc, char *argv[])
{
	int n, nt, max = 1;
	double ti, ts, tp;
	double *v1 = NULL, *v2 = NULL;
	
	#if defined (_OPENMP)
		max = omp_get_max_threads();
	#endif
	
	if(argc < 3) {
		fprintf(stderr, "Usage: %s <size> <omp_threads>\n", argv[0]);
		return(EXIT_FAILURE);
	}
	n  = atoi(argv[1]);
	nt = (max > 1) ? atoi(argv[2]) : max;
	
	if((n < 0) || (nt < 1)) {
		fprintf(stderr, "Invalid Value for Parameters.\n");
		fprintf(stderr, "Exiting...\n");
		return(EXIT_FAILURE);
	}
	
	v1 = (double *) malloc(sizeof(double)*n);
	v2 = (double *) malloc(sizeof(double)*n);
	
	init_data(v1, n);
	memcpy(v2, v1, sizeof(double)*n);
	
	ti = get_time();
	merge_sec(v1,n);	// version secuencial
	ts = (get_time() - ti);
	
	printf("\n");
	printf("Tiempo Secuencial: %.4lf segundos\n", ts);
	
	#if defined (_OPENMP)
		omp_set_num_threads(nt);
	#endif
	ti = get_time();
	#pragma omp parallel
	#pragma omp single nowait
	merge_par(v2,n,nt);	// version paralela
	tp = (get_time() - ti);
	
	printf("Tiempo Paralelo:   %.4lf segundos\n", tp);
	printf("\n");

	printf("Speed-Up =  %.2lf\n", ts/tp);
	printf("\n");
	
	printf("Checking Result...");
	if (compare(v1, v2, n) == 0) {
		printf(" Success");
	}
	else {
		printf(" Fail");
	}
	printf("\n");
	
	free(v1);
	free(v2);
	printf("\n");
	return(EXIT_SUCCESS);
}
