#if defined (_OPENMP)
	#include <omp.h>
#endif

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int x = 100;
	int tid, nt, max = 1;
	char str[12] = "Hello World";
	
	#if defined (_OPENMP)
		max = omp_get_max_threads();
	#endif
	
	if(argc < 2) {
		fprintf(stderr, "Usage: %s <omp_threads>\n", argv[0]);
		return(EXIT_FAILURE);
	}
	nt = (max > 1) ? atoi(argv[1]) : max;
	
	if(nt < 1) {
		fprintf(stderr, "Invalid Value for OMP_THREADS...\n");
		return(EXIT_FAILURE);
	}
	
	printf("\n");
	printf("MAX_OMP_THREADS: %d\n", max);
	printf("OMP_THREADS: %d\n", nt);
	printf("\n");

	
	#pragma omp parallel sections shared(str) private(tid) reduction(+:x) num_threads(nt)
	{
		#if defined (_OPENMP)
			tid = omp_get_thread_num();
		#endif

		#pragma omp section
		{
			x += tid;
			printf("%s from Thread %d inside Section 1 (x = %d)\n", str, tid, x);
		}
		
		#pragma omp section
		{
			x += tid;
			printf("%s from Thread %d inside Section 2 (x = %d)\n", str, tid, x);
		}
	}
	
	#pragma omp parallel private(tid) num_threads(nt)
	{
		#if defined (_OPENMP)
			tid = omp_get_thread_num();
		#endif

		#pragma omp single
		{
			x += tid;
			printf("Thread %d out of Sections... (x = %d)\n", tid, x);
		}
	}
	printf("\n");
	printf("After Parallel: x = %d\n", x);

	printf("\n");
	return(EXIT_SUCCESS);
}
