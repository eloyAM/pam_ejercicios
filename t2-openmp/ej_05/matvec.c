#if defined (_OPENMP)
	#include <omp.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
#include <unistd.h>

void init_data(double *d, int size)
{
	int i;
	srand(getpid());
	
	for(i=0; i<size; i++) {
		d[i] = (double) rand()/RAND_MAX;
	}
}

double compare(double *v1, double *v2, int n)
{
	int i;
	double diff = 0.0;
	
	for(i=0; i<n; i++) {
		diff += fabs(v1[i] - v2[i]);
	}
	return diff;
}

void mv_sec(double *m, int fm, int cm, double *v, double *vr)
{
	for(int i=0; i<fm; i++) {
		double s = 0.0;
		for(int j=0; j<cm; j++) {
			s += m[i*cm + j] * v[j];
		}
		vr[i] = s;
	}
}

// __attribute__((optimize("no-tree-vectorize")))
void mv_par(double *m, int fm, int cm, double *v, double *vr) {
	#pragma omp parallel for
	for (int i = 0; i < fm; i++) {
		double s = 0.0;
		#pragma omp simd reduction(+:s)
		for (int j = 0; j < cm; j++) {
			s += m[i * cm + j] * v[j];
		}
		vr[i] = s;
	}
}

double get_time( void ) {
    struct timeval t;
    gettimeofday (&t, (struct timezone *)0);
    return (((double) t.tv_sec) + ((double) t.tv_usec)*1E-6);
}

int main(int argc, char *argv[])
{
	int nt, max = 1;
	double ti, ts, tp;
	double *m, *v, *vr1, *vr2;
	
	#if defined (_OPENMP)
		max = omp_get_max_threads();
	#endif
	
	if(argc < 4) {
		fprintf(stderr, "Usage: %s <rows_m> <cols_m> <omp_threads>\n", argv[0]);
		return(-1);
	}
	int fm = atoi(argv[1]);
	int cm = atoi(argv[2]);
	assert((fm > 0) && (cm > 0));
	
	nt = (max > 1) ? atoi(argv[3]) : max;
	assert(nt > 0);
	
	int size = fm*cm;
	
	m = (double *) malloc(sizeof(double)*size);
	v = (double *) malloc(sizeof(double)*cm);
	
	vr1 = (double *) malloc(sizeof(double)*cm);
	vr2 = (double *) malloc(sizeof(double)*cm);
	
	/* Initialize Matrix and Vector */
	init_data(m, size);
	init_data(v,   cm);
	
	ti = get_time();
	mv_sec(m, fm, cm, v, vr1); // version secuencial
	ts = get_time() - ti;
	
	printf("\n");
	printf("Tiempo Secuencial: %.2lf segundos\n", ts);
	
	#if defined (_OPENMP)
		omp_set_num_threads(nt);
	#endif
	
	ti = get_time();
	mv_par(m, fm, cm, v, vr2); // version paralela
	tp = get_time() - ti;
	
	printf("Tiempo Paralelo:   %.2lf segundos\n", tp);
	printf("\n");
	
	printf("Diferencia: %.2lf\n", compare(vr1, vr2, cm));
	printf("Speed-Up =  %.2lf\n", ts/tp);
	printf("\n");
	
	/* Free Allocated Memory */
	free(m); free(v);
	free(vr2); free(vr1);
	
	return(EXIT_SUCCESS);
}
