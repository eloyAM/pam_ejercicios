#if defined (_OPENMP)
	#include <omp.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define N_OCUR 5

void init_data(double *v, int n)
{
	int i;
	srand(getpid());
	
	for(i=0; i<n; i++) {
		v[i] = (double) rand()/RAND_MAX;
	}
}

int search_sec(double *v, int n, double elem)
{
	int position = -1;
	
	for(int i=0; i<n; i++) {
		if (v[i] == elem) {
			position = i;
			break;
		}
	}
	return position;
}

int search_par(double *v, int n, double elem, int *h, int nthreads) {
	if (n < 1)
		return -1;
	if (nthreads == 1)
	{
		int position = search_sec(v, n, elem);
		if (position != -1)
		{
			#if defined (_OPENMP)
				*h = omp_get_thread_num();
			#endif
		}
		return position;
	}
	else
	{
		int res_izq, res_der;

		#pragma omp task shared(res_izq)
		res_izq = search_par(v, n/2, elem, h, nthreads/2);

		#pragma omp task shared(res_der)
		res_der = search_par(&v[n/2], n-n/2, elem, h, nthreads/2);

		#pragma omp taskwait
		{
			if (res_izq != -1)
			{
				#if defined (_OPENMP)
					*h = omp_get_thread_num();
				#endif
				return res_izq;	
			}
			if (res_der != -1)
			{
				#if defined (_OPENMP)
					*h = omp_get_thread_num();
				#endif
				return res_der;
			}
		}
	}
	return -1;
}

int main(int argc, char *argv[])
{
	int i, h, n, max = 1, nt;
	double *v = NULL;
	
	#if defined (_OPENMP)
		max = omp_get_max_threads();
	#endif
	
	if(argc < 3) {
		fprintf(stderr, "Usage: %s <size> <omp_threads>\n", argv[0]);
		return(EXIT_FAILURE);
	}
	n  = atoi(argv[1]);
	nt = (max > 1) ? atoi(argv[2]) : max;
	
	if((n < 0) || (nt < 1)) {
		fprintf(stderr, "Invalid Value for Parameters.\n");
		fprintf(stderr, "Exiting...\n");
		return(EXIT_FAILURE);
	}
	
	v = (double *) malloc(sizeof(double)*n);
	init_data(v, n);
	
	i = rand() % n;
	double value = v[i];
	
	int j;
	printf("Positions: %d ", i);
	for(i=0; i<(N_OCUR-1); i++) {
		j = rand() % n;
		v[j] = value;
		printf("%d ", j);
	}
	printf("\n");	
	printf("Value = %1.4lf\n", value);
	
	printf("\n");
	i  = search_sec(v, n, value); // version secuencial
	printf("Sec_Position = %d\n", i);

	#if defined (_OPENMP)
		omp_set_num_threads(nt);
	#endif
	
	printf("\n");
	#pragma omp parallel
	#pragma omp single nowait
	i  = search_par(v, n, value, &h, nt); // version paralela
	printf("Par_Position = %d (", i);
	printf("Found by Thread %d)\n", h);
	
	free(v);
	printf("\n");
	return(EXIT_SUCCESS);
}
