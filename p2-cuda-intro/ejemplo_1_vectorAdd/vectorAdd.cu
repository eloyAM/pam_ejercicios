/**
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/**
 * Vector addition: C = A + B.
 *
 * This sample is a very basic sample that implements element by element
 * vector addition. It is the same as the sample illustrating Chapter 2
 * of the programming guide with some additions like error checking.
 */

#include <stdio.h>

// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>
 
// Aux functions
#include <helper_cuda.h>
 
/**
 * CUDA Kernel Device code
 *
 * Computes the vector addition of A and B into C. The 3 vectors have the same
 * number of elements numElements.
 */
__global__ void
vectorAdd(const float *A, const float *B, float *suma, float *resta, int numElements)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
 
    if (i < numElements)
    {
        suma[i] = A[i] + B[i];
        resta[i] = A[i] - B[i];
    }
}
 
/**
 * Host main routine
 */
int
main(void)
{
    // Print the vector length to be used, and compute its size
    const int numElements = 4096;
    const size_t size = numElements * sizeof(float);
    printf("[Vector addition of %d elements]\n", numElements);
 
    // Host vectors
    float *h_A, *h_B, *h_C, *h_D;
 
    // Allocate the host input vector A
    h_A = (float *)malloc(size);
    // Allocate the host input vector B
    h_B = (float *)malloc(size);
    // Allocate the host output vector C
    h_C = (float *)malloc(size);
    // Allocate the host output vector D
    h_D = (float *)malloc(size);
 
    // Verify that allocations succeeded
    if (h_A == NULL || h_B == NULL || h_C == NULL || h_D == NULL)
    {
        fprintf(stderr, "Failed to allocate host vectors!\n");
        exit(EXIT_FAILURE);
    }
 
    // Initialize the host input vectors
    for (int i = 0; i < numElements; ++i)
    {
        h_A[i] = rand()/(float)RAND_MAX;
        h_B[i] = rand()/(float)RAND_MAX;
    }
 
    // Device vectors
    float *d_A = NULL, *d_B = NULL, *d_C = NULL, *d_D = NULL;
 
    // Allocate the device input vector A
    checkCudaErrors(cudaMalloc((void **)&d_A, size));
    // Allocate the device input vector B
    checkCudaErrors(cudaMalloc((void **)&d_B, size));
    // Allocate the device output vector C
    checkCudaErrors(cudaMalloc((void **)&d_C, size));
    // Allocate the device output vector D
    checkCudaErrors(cudaMalloc((void **)&d_D, size));
 
    // Copy the host input vectors A and B in host memory to the device input vectors in
    // device memory
    printf("Copy input data from the host memory to the CUDA device\n");
    checkCudaErrors(cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_B, h_B, size, cudaMemcpyHostToDevice));
 
    // Launch the Vector Add CUDA Kernel
    const int threadsPerBlock = 256;
    const int blocksPerGrid = (numElements + threadsPerBlock - 1) / threadsPerBlock;
    printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, threadsPerBlock);
    vectorAdd<<<blocksPerGrid, threadsPerBlock>>>(d_A, d_B, d_C, d_D, numElements);
    checkCudaErrors(cudaGetLastError());
 
    // Copy the device result vector in device memory to the host result vector in host memory.
    printf("Copy output data from the CUDA device to the host memory\n");
    checkCudaErrors(cudaMemcpy(h_C, d_C, size, cudaMemcpyDeviceToHost));
 
    // Copy the device result vector in device memory to the host result vector
    // in host memory.
    printf("Copy output data from the CUDA device to the host memory\n");
    checkCudaErrors(cudaMemcpy(h_D, d_D, size, cudaMemcpyDeviceToHost));
 
    // Verify that the result vector is correct
    for (int i = 0; i < numElements; ++i)
    {
        if (fabs(h_A[i] + h_B[i] - h_C[i]) > 1e-5)
        {
           fprintf(stderr, "Result verification of sum failed at element %d!\n", i);
           exit(EXIT_FAILURE);
        }
        if (fabs(h_A[i] - h_B[i] - h_D[i]) > 1e-5)
        {
           fprintf(stderr, "Result verification of sub failed at element %d!\n", i);
           exit(EXIT_FAILURE);
        }
    }
 
    printf("Test PASSED\n");
 
    // Free device global memory
    checkCudaErrors(cudaFree(d_A));
    checkCudaErrors(cudaFree(d_B));
    checkCudaErrors(cudaFree(d_C));
    checkCudaErrors(cudaFree(d_D));
 
    // Free host memory
    free(h_A);
    free(h_B);
    free(h_C);
    free(h_D);
 
    // Reset the device and exit
    // cudaDeviceReset causes the driver to clean up all state. While
    // not mandatory in normal operation, it is good practice.  It is also
    // needed to ensure correct operation when the application is being
    // profiled. Calling cudaDeviceReset causes all profile data to be
    // flushed before the application exits
    checkCudaErrors(cudaDeviceReset());
 
    printf("Done\n");
    return 0;
}
 
 