#include <stdio.h>
#include <assert.h>
#include <sys/types.h>

// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>

// Aux functions
#include <helper_cuda.h>

// OpenMP
#include <omp.h>

// Max size of W dimension
#define TILE_DIM 32
// Max num of shared memory elements 
#define SHMEM (TILE_DIM * TILE_DIM)
// Max num of bytes on shared memory
#define SHMEM_BYTES (sizeof(float) * SHMEM)
// Minimum of two elements
#define MIN(a,b) (((a)<(b))?(a):(b))


__global__ void
mulmat_1CmG(const float *a, const float *b, float *c, uint N, uint K, uint W, uint R)
{
    __shared__ float aTile[TILE_DIM][TILE_DIM]; // W*W elements
    __shared__ float bTile[TILE_DIM][TILE_DIM]; // W*W elements
    uint row = blockIdx.y * blockDim.y + threadIdx.y;
    uint col = blockIdx.x * blockDim.x + threadIdx.x;
    float sum = 0.0f;

    for (uint tile = 0; tile < R; ++tile) {
        aTile[threadIdx.y][threadIdx.x] = a[row * K + (tile * W + threadIdx.x)];
        bTile[threadIdx.y][threadIdx.x] = b[(tile * W + threadIdx.y) * N + col];
        __syncthreads();

        for (uint k = 0; k < W; ++k) {
            sum += aTile[threadIdx.y][k] * bTile[k][threadIdx.x];
        }
        __syncthreads();
    }

    c[row * N + col] = sum;
}

// Basic CPU matrix multiplication
void mulmat_CPU(const float *a, const float *b, float *c, uint F, uint M, uint N, uint K)
{
    #pragma omp parallel for schedule(static) collapse(2)
    for (uint i = M-F; i < M; ++i) {
        for (uint j = 0; j < N; ++j) {
            float sum = 0.0f;
//            #pragma omp simd reduction(+:sum)
            for (uint k = 0; k < K; ++k) {
                sum += a[i * K + k] * b[k * N + j];
            }
            c[i * N + j] = sum;
        }
    }
}

// Tiled CPU matrix multiplication
void mulmat_CPU_tiled(const float *a, const float *b, float *c, uint F, uint M, uint N, uint K)
{
    #pragma omp for schedule(static) collapse(3)
    for (uint ii = M-F; ii < M; ii += TILE_DIM) {
        for (uint jj = 0; jj < N; jj += TILE_DIM) {
            for (uint kk = 0; kk < K; kk += TILE_DIM) {
                for (uint i = ii; i < MIN(ii + TILE_DIM, M); i++) {
                    for (uint j = jj; j < MIN(jj + TILE_DIM, N); j++) {
                        float sum = 0.0f;
                        for (uint k = kk; k < MIN(kk + TILE_DIM, K); k++) {
                            sum += a[i * K + k] * b[k * N + j];
                        }
                        c[i * N + j] += sum;
                    }
                }
            }
        }
    }
}

void checkResult(const float *a, const float *b, float *c, int M, int N, int K)
{
    for (int i = 0; i < M; ++i) {
        for (int j = 0; j < N; ++j) {
            float expected = 0.0f;
            for (int k = 0; k < K; ++k) {
                expected += a[i * K + k] * b[k * N + j];
            }
            float result = c[i * N + j];
            if (fabs(expected - result) > 1e-5) {
                fprintf(stderr, "Validation failed at element C[%d][%d]\n", i, j);
                fprintf(stderr, "Got %f Expected %f\n", result, expected);
                exit(EXIT_FAILURE);
            }
        }
    }
}

void inicializarMatrices(int M, int N, int K, float *h_a, float *h_b)
{
    for (int i = 0; i < M; ++i) {
        for (int j = 0; j < K; ++j) {
            h_a[i * K + j] = rand()%100;
        }
    }

    for (int i = 0; i < K; ++i) {
        for (int j = 0; j < N; ++j) {
            h_b[i * N + j] = rand()%100;
        }
    }
}

void checkDeviceCount(int D)
{
    int deviceCount;
    checkCudaErrors(cudaGetDeviceCount(&deviceCount));
    assert(D <= deviceCount);
}


int main(int argc, char **argv)
{
    const int W = getCmdLineArgumentInt(argc, (const char**) argv, "W")? : TILE_DIM;
    const int M = getCmdLineArgumentInt(argc, (const char**) argv, "M")? : W * 8;
    const int N = getCmdLineArgumentInt(argc, (const char**) argv, "N")? : W * 6;
    const int K = getCmdLineArgumentInt(argc, (const char**) argv, "K")? : W * 10;
    const int F = getCmdLineArgumentInt(argc, (const char**) argv, "F")? : 0;
    const int Q = getCmdLineArgumentInt(argc, (const char**) argv, "Q")? : omp_get_max_threads();
    const int D = getCmdLineArgumentInt(argc, (const char**) argv, "D")? : 1;
    checkDeviceCount(D); assert((M-F) % D == 0); assert((M-F)/D % W == 0);
    assert(M%W==0 && N%W==0 && K%W==0); assert(F%W==0 && F<M);// Simplificar el problema
    const uint S=((M-F)/D)/W, T=N/W, R=K/W;
    dim3 gridDim(T, S), blockDim(W, W);
    printf("M=%d, N=%d, K=%d, W=%d, TILE_DIM=%d", M, N, K, W, TILE_DIM);
    printf(", R=%d, S=%d, T=%d, F=%d, Threads=%d, Devices=%d\n", R, S, T, F, Q, D);
    printf("Kernel. Grid(x=%d, y=%d) Blocks(x=%d, y=%d)\n", gridDim.x, gridDim.y, blockDim.x, blockDim.y);
    const size_t aMatrixBytes = M * K * sizeof(float);
    const size_t bMatrixBytes = K * N * sizeof(float);
    const size_t resultMatrixBytes = M * N * sizeof(float);
    const size_t d_aMatrixBytes = ((M-F) * K)/D * sizeof(float);
    const size_t d_resultMatrixBytes = ((M-F) * N)/D * sizeof(float);

    // HOST matrices
    float *h_a=NULL, *h_b=NULL, *h_c=NULL;
    h_a = (float*) malloc(aMatrixBytes);
    h_b = (float*) malloc(bMatrixBytes);
    h_c = (float*) malloc(resultMatrixBytes);

    // Initialize input
    inicializarMatrices(M, N, K, h_a, h_b);

    // Array of pointers to HOST matrices
    float *h_a_slice[D], *h_c_slice[D];
    for (int i = 0; i < D; ++i) {
        h_a_slice[i] = h_a + i*(((M-F)*K)/D);
        h_c_slice[i] = h_c + i*(((M-F)*N)/D);
    }

    // DEVICE matrices
    float *d_a[D], *d_b[D], *d_c[D];
    #pragma omp parallel for num_threads(D)
    for (int i = 0; i < D; ++i) {
        checkCudaErrors(cudaSetDevice(i));
        checkCudaErrors(cudaMalloc(&d_a[i], d_aMatrixBytes));
        checkCudaErrors(cudaMalloc(&d_b[i], bMatrixBytes));
        checkCudaErrors(cudaMalloc(&d_c[i], d_resultMatrixBytes));
    }

    // Computing and memory transfer time
    double startTime, stopTime;

    // Start time recording
    startTime = omp_get_wtime();

#pragma omp parallel num_threads(Q)
{
    /*GPU matrix multiplication*/
    #pragma omp for nowait
    for (int i = 0; i < D; ++i)
    {
        // Multi-GPU
        checkCudaErrors(cudaSetDevice(i));
        // Copy input data to device
        checkCudaErrors(cudaMemcpy(d_a[i], h_a_slice[i], d_aMatrixBytes, cudaMemcpyHostToDevice));
        checkCudaErrors(cudaMemcpy(d_b[i], h_b, bMatrixBytes, cudaMemcpyHostToDevice));
        // KERNEL EXECUTION
        //grid.y=S=((M-F)/D)/W, grid.x=T=N/W, tiles=R=K/W, blocks.y=W, blocks.x=W
        mulmat_1CmG<<<gridDim, blockDim>>>(d_a[i], d_b[i], d_c[i], N, K, W, R);
        checkCudaErrors(cudaGetLastError());
        // Bring back output data from device
        checkCudaErrors(cudaMemcpy(h_c_slice[i], d_c[i], d_resultMatrixBytes, cudaMemcpyDeviceToHost));
    }

    /*CPU matrix multiplication*/
    #pragma omp for schedule(static) collapse(2) nowait
    for (uint i = M-F; i < M; ++i) {
        for (uint j = 0; j < N; ++j) {
            float sum = 0.0f;
//            #pragma omp simd reduction(+:sum)
            for (uint k = 0; k < K; ++k) {
                sum += h_a[i * K + k] * h_b[k * N + j];
            }
            h_c[i * N + j] = sum;
        }
    }
}
    // Stop time recording
    stopTime = omp_get_wtime();
    double time = stopTime - startTime;
    printf("Time: %f ms\n", time*1000);

    // Check result
    printf("Check result\n");
    checkResult(h_a, h_b, h_c, M, N, K);

    /* FINALIZE PROGRAM */

    // Free host global memory
    free(h_a);
    free(h_b);
    free(h_c);

    // Free device global memory
    #pragma omp parallel for num_threads(D)
    for (int i = 0; i < D; ++i) {
        checkCudaErrors(cudaSetDevice(i));
        checkCudaErrors(cudaFree(d_a[i]));
        checkCudaErrors(cudaFree(d_b[i]));
        checkCudaErrors(cudaFree(d_c[i]));
    }

    // Reset the device and exit
    checkCudaErrors(cudaDeviceReset());

    printf("Done\n");
    return EXIT_SUCCESS;
}
