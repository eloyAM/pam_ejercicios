#include <stdio.h>
#include <assert.h>
#include <sys/types.h>

// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>

// Aux functions
#include <helper_cuda.h>

// OpenMP
#include <omp.h>

// Max size of W dimension
#define TILE_DIM 32
// Max num of shared memory elements 
#define SHMEM (TILE_DIM * TILE_DIM)
// Max num of bytes on shared memory
#define SHMEM_BYTES (sizeof(float) * SHMEM)
// Minimum of two elements
#define MIN(a,b) (((a)<(b))?(a):(b))


__global__ void
mulmat_1C1G(const float *a, const float *b, float *c, uint N, uint K, uint W, uint R)
{
    __shared__ float aTile[TILE_DIM][TILE_DIM]; // W*W elements
    __shared__ float bTile[TILE_DIM][TILE_DIM]; // W*W elements
    uint row = blockIdx.y * blockDim.y + threadIdx.y;
    uint col = blockIdx.x * blockDim.x + threadIdx.x;
    float sum = 0.0f;

    for (uint tile = 0; tile < R; ++tile) {
        aTile[threadIdx.y][threadIdx.x] = a[row * K + (tile * W + threadIdx.x)];
        bTile[threadIdx.y][threadIdx.x] = b[(tile * W + threadIdx.y) * N + col];
        __syncthreads();

        for (uint k = 0; k < W; ++k) {
            sum += aTile[threadIdx.y][k] * bTile[k][threadIdx.x];
        }
        __syncthreads();
    }

    c[row * N + col] = sum;
}

// Basic CPU matrix multiplication
void mulmat_CPU(const float *a, const float *b, float *c, uint F, uint M, uint N, uint K)
{
    #pragma omp parallel for schedule(static) collapse(2)
    for (uint i = M-F; i < M; ++i) {
        for (uint j = 0; j < N; ++j) {
            float sum = 0.0f;
//            #pragma omp simd reduction(+:sum)
            for (uint k = 0; k < K; ++k) {
                sum += a[i * K + k] * b[k * N + j];
            }
            c[i * N + j] = sum;
        }
    }
}

// Tiled CPU matrix multiplication
void mulmat_CPU_tiled(const float *a, const float *b, float *c, uint F, uint M, uint N, uint K)
{
    #pragma omp for schedule(static) collapse(3)
    for (uint ii = M-F; ii < M; ii += TILE_DIM) {
        for (uint jj = 0; jj < N; jj += TILE_DIM) {
            for (uint kk = 0; kk < K; kk += TILE_DIM) {
                for (uint i = ii; i < MIN(ii + TILE_DIM, M); i++) {
                    for (uint j = jj; j < MIN(jj + TILE_DIM, N); j++) {
                        float sum = 0.0f;
                        for (uint k = kk; k < MIN(kk + TILE_DIM, K); k++) {
                            sum += a[i * K + k] * b[k * N + j];
                        }
                        c[i * N + j] += sum;
                    }
                }
            }
        }
    }
}

void checkResult(const float *a, const float *b, float *c, int M, int N, int K)
{
    for (int i = 0; i < M; ++i) {
        for (int j = 0; j < N; ++j) {
            float expected = 0.0f;
            for (int k = 0; k < K; ++k) {
                expected += a[i * K + k] * b[k * N + j];
            }
            float result = c[i * N + j];
            if (fabs(expected - result) > 1e-5) {
                fprintf(stderr, "Validation failed at element C[%d][%d]\n", i, j);
                fprintf(stderr, "Got %f Expected %f\n", result, expected);
                exit(EXIT_FAILURE);
            }
        }
    }
}

void inicializarMatrices(int M, int N, int K, float *h_a, float *h_b)
{
    for (int i = 0; i < M; ++i) {
        for (int j = 0; j < K; ++j) {
            h_a[i * K + j] = rand()%100;
        }
    }

    for (int i = 0; i < K; ++i) {
        for (int j = 0; j < N; ++j) {
            h_b[i * N + j] = rand()%100;
        }
    }
}


int main(int argc, char **argv)
{
    const int W = getCmdLineArgumentInt(argc, (const char**) argv, "W")? : TILE_DIM;
    const int M = getCmdLineArgumentInt(argc, (const char**) argv, "M")? : W * 8;
    const int N = getCmdLineArgumentInt(argc, (const char**) argv, "N")? : W * 6;
    const int K = getCmdLineArgumentInt(argc, (const char**) argv, "K")? : W * 10;
    const int F = getCmdLineArgumentInt(argc, (const char**) argv, "F")? : 0;
    const int Q = getCmdLineArgumentInt(argc, (const char**) argv, "Q")? : omp_get_max_threads();
    assert(M%W==0 && N%W==0 && K%W==0); assert(F%W==0 && F<M);// Simplificar el problema
    const uint S=(M-F)/W, T=N/W, R=K/W;
    dim3 gridDim(T, S), blockDim(W, W);
    printf("M=%d, N=%d, K=%d, W=%d, TILE_DIM=%d", M, N, K, W, TILE_DIM);
    printf(", R=%d, S=%d, T=%d, F=%d, Threads=%d\n", R, S, T, F, Q);
    printf("Kernel. Grid(x=%d, y=%d) Blocks(x=%d, y=%d)\n", gridDim.x, gridDim.y, blockDim.x, blockDim.y);
    const size_t aMatrixBytes = M * K * sizeof(float);
    const size_t bMatrixBytes = K * N * sizeof(float);
    const size_t resultMatrixBytes = M * N * sizeof(float);
    const size_t d_aMatrixBytes = (M-F) * K * sizeof(float);
    const size_t d_resultMatrixBytes = (M-F) * N * sizeof(float);

    // HOST matrices
    float *h_a=NULL, *h_b=NULL, *h_c=NULL;
    h_a = (float*) malloc(aMatrixBytes);
    h_b = (float*) malloc(bMatrixBytes);
    h_c = (float*) malloc(resultMatrixBytes);

    // Initialize input
    inicializarMatrices(M, N, K, h_a, h_b);

    // DEVICE matrices
    float *d_a=NULL, *d_b=NULL, *d_c=NULL;
    checkCudaErrors(cudaMalloc(&d_a, d_aMatrixBytes)); //(void **)&d_a
    checkCudaErrors(cudaMalloc(&d_b, bMatrixBytes));
    checkCudaErrors(cudaMalloc(&d_c, d_resultMatrixBytes));

    // Computing and memory transfer time
    cudaEvent_t startEvent, stopEvent;
    checkCudaErrors(cudaEventCreate(&startEvent));
    checkCudaErrors(cudaEventCreate(&stopEvent));

    // Start time recording
    checkCudaErrors(cudaEventRecord(startEvent));

#pragma omp parallel num_threads(Q)
{
    /*GPU matrix multiplication*/
    #pragma omp single nowait
    {
    // Copy input data to device
    checkCudaErrors(cudaMemcpy(d_a, h_a, d_aMatrixBytes, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_b, h_b, bMatrixBytes, cudaMemcpyHostToDevice));

    // KERNEL EXECUTION
    //grid.y=S=(M-F)/W, grid.x=T=N/W, tiles=R=K/W, blocks.y=W, blocks.x=W
    mulmat_1C1G<<<gridDim, blockDim>>>(d_a, d_b, d_c, N, K, W, R);
    checkCudaErrors(cudaGetLastError());

    // Bring back output data from device
    checkCudaErrors(cudaMemcpy(h_c, d_c, d_resultMatrixBytes, cudaMemcpyDeviceToHost));
    }

    /*CPU matrix multiplication*/
    #pragma omp for schedule(static) collapse(2)
    for (uint i = M-F; i < M; ++i) {
        for (uint j = 0; j < N; ++j) {
            float sum = 0.0f;
//            #pragma omp simd reduction(+:sum)
            for (uint k = 0; k < K; ++k) {
                sum += h_a[i * K + k] * h_b[k * N + j];
            }
            h_c[i * N + j] = sum;
        }
    }
}
    // Stop time recording
    checkCudaErrors(cudaEventRecord(stopEvent));
    checkCudaErrors(cudaEventSynchronize(stopEvent));
    float time = -1.0f;
    checkCudaErrors(cudaEventElapsedTime(&time, startEvent, stopEvent));
    printf("Time: %f ms\n", time);

    // Check result
    printf("Check result\n");
    checkResult(h_a, h_b, h_c, M, N, K);

    /* FINALIZE PROGRAM */

    // Free host global memory
    free(h_a);
    free(h_b);
    free(h_c);

    // Free device global memory
    checkCudaErrors(cudaFree(d_a));
    checkCudaErrors(cudaFree(d_b));
    checkCudaErrors(cudaFree(d_c));
    // Destroy events
    checkCudaErrors(cudaEventDestroy(startEvent));
    checkCudaErrors(cudaEventDestroy(stopEvent));

    // Reset the device and exit
    checkCudaErrors(cudaDeviceReset());

    printf("Done\n");
    return EXIT_SUCCESS;
}
