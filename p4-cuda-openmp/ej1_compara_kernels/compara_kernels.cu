#include <stdio.h>
#include <assert.h>
#include <sys/types.h>

// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>

// Aux functions
#include <helper_cuda.h>

// Max size of W dimension
#define TILE_DIM 32
// Max num of shared memory elements 
#define SHMEM (TILE_DIM * TILE_DIM)
// Max num of bytes on shared memory
#define SHMEM_BYTES (sizeof(float) * SHMEM)

__global__ void
simpleMultiply(const float *a, const float *b, float *c, uint N, uint W)
{
    uint row = blockIdx.y * blockDim.y + threadIdx.y;
    uint col = blockIdx.x * blockDim.x + threadIdx.x;
    float sum = 0.0f;

    for (uint k = 0; k < W; ++k) {
        sum += a[row * W + k] * b[k * N + col];
    }

    c[row * N + col] = sum;
}

__global__ void
coalescedMultiply(const float *a, const float *b, float *c, uint N, uint W)
{
    __shared__ float aTile[TILE_DIM][TILE_DIM]; // W*W elements
    uint row = blockIdx.y * blockDim.y + threadIdx.y;
    uint col = blockIdx.x * blockDim.x + threadIdx.x;
    float sum = 0.0f;

    aTile[threadIdx.y][threadIdx.x] = a[row * W + threadIdx.x];

    for (uint k = 0; k < W; ++k) {
        sum += aTile[threadIdx.y][k] * b[k * N + col];
    }

    c[row * N + col] = sum;
}

__global__ void
sharedABMultiply(const float *a, const float *b, float *c, uint N, uint W)
{
    __shared__ float aTile[TILE_DIM][TILE_DIM]; // W*W elements
    __shared__ float bTile[TILE_DIM][TILE_DIM]; // W*W elements
    uint row = blockIdx.y * blockDim.y + threadIdx.y;
    uint col = blockIdx.x * blockDim.x + threadIdx.x;
    float sum = 0.0f;

    aTile[threadIdx.y][threadIdx.x] = a[row * W + threadIdx.x];
    bTile[threadIdx.y][threadIdx.x] = b[threadIdx.y * N + col];
    __syncthreads();

    for (uint k = 0; k < W; ++k) {
        sum += aTile[threadIdx.y][k] * bTile[k][threadIdx.x];
    }

    c[row * N + col] = sum;
}

// Para nuestro caso M=N, K=W
void checkResult(const float *a, const float *b, float *c, int M, int N, int K)
{
    for (int i = 0; i < M; ++i) {
        for (int j = 0; j < N; ++j) {
            float expected = 0.0f;
            for (int k = 0; k < K; ++k) {
                expected += a[i * K + k] * b[k * N + j];
            }
            float result = c[i * N + j];
            if (fabs(expected - result) > 1e-5) {
                fprintf(stderr, "Validation failed at element C[%d][%d]\n", i, j);
                fprintf(stderr, "Got %f Expected %f\n", result, expected);
                exit(EXIT_FAILURE);
            }
        }
    }
}

// Para nuestro caso M=N, K=W
void inicializarMatrices(int M, int N, int K, float *h_a, float *h_b)
{
    for (int i = 0; i < M; ++i) {
        for (int j = 0; j < K; ++j) {
            h_a[i * K + j] = i;
        }
    }

    for (int i = 0; i < K; ++i) {
        for (int j = 0; j < N; ++j) {
            h_b[i * N + j] = 1;
        }
    }
}

enum kernelType {simple, coalesced, sharedAB};

const char* kernelTypeName(int kernel)
{
    switch (kernel) {
        case simple: return "simple";
        case coalesced: return "coalesced";
        case sharedAB: return "sharedAB";
        default: return "unknown";
    }
}


int main(int argc, char **argv)
{
    const int W = getCmdLineArgumentInt(argc, (const char**) argv, "W")? : TILE_DIM;
    const int N = getCmdLineArgumentInt(argc, (const char**) argv, "N")? : W * 2;
    assert(N % W == 0); // Simplificar el problema
    const int K = getCmdLineArgumentInt(argc, (const char**) argv, "K")? : simple;
    if (K == coalesced) {assert(TILE_DIM % W == 0);}
    const int T = N / W;
    printf("N=%d, W=%d, T=%d, TILE_DIM=%d\n", N, W, T, TILE_DIM);
    const size_t inputMatrixBytes = N * W * sizeof(float);
    const size_t resultMatrixBytes = N * N * sizeof(float);

    // HOST matrices
    float *h_a=NULL, *h_b=NULL, *h_c=NULL;
    h_a = (float*) malloc(inputMatrixBytes);
    h_b = (float*) malloc(inputMatrixBytes);
    h_c = (float*) malloc(resultMatrixBytes);
    // Initialize input
    inicializarMatrices(N, N, W, h_a, h_b);

    // DEVICE matrices
    float *d_a=NULL, *d_b=NULL, *d_c=NULL;
    checkCudaErrors(cudaMalloc(&d_a, inputMatrixBytes)); //(void **)&d_a
    checkCudaErrors(cudaMalloc(&d_b, inputMatrixBytes));
    checkCudaErrors(cudaMalloc(&d_c, resultMatrixBytes));

    // Copy input data to device
    printf("Copy memory from host to decive\n");
    checkCudaErrors(cudaMemcpy(d_a, h_a, inputMatrixBytes, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_b, h_b, inputMatrixBytes, cudaMemcpyHostToDevice));

    // KERNEL EXECUTION
    dim3 gridDim(T, T);
    dim3 blockDim(W, W);
    printf("Kernel %s. Grid: %dx%d, Blocks: %dx%d\n", kernelTypeName(K),
            gridDim.x, gridDim.y, blockDim.x, blockDim.y);
    switch (K) {
        case simple:
            simpleMultiply<<<gridDim, blockDim>>>(d_a, d_b, d_c, N, W);
            break;
        case coalesced:
            coalescedMultiply<<<gridDim, blockDim>>>(d_a, d_b, d_c, N, W);
            break;
        case sharedAB:
            sharedABMultiply<<<gridDim, blockDim>>>(d_a, d_b, d_c, N, W);
            break;
        default:
            fprintf(stderr, "Invalid kernel type. Options: 0=simple, 1=coalesced, 2=sharedAB");
            exit(EXIT_FAILURE);
    }
    checkCudaErrors(cudaGetLastError());

    // Bring back output data from device
    printf("Copy memory from device to host\n");
    checkCudaErrors(cudaMemcpy(h_c, d_c, resultMatrixBytes, cudaMemcpyDeviceToHost));

    // Check result
    printf("Check result\n");
    checkResult(h_a, h_b, h_c, N, N, W);

    /* FINALIZE PROGRAM */

    // Free host global memory
    free(h_a);
    free(h_b);
    free(h_c);

    // Free device global memory
    checkCudaErrors(cudaFree(d_a));
    checkCudaErrors(cudaFree(d_b));
    checkCudaErrors(cudaFree(d_c));

    // Reset the device and exit
    checkCudaErrors(cudaDeviceReset());

    printf("Done\n");
    return EXIT_SUCCESS;
}
