#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

void inicializar_matriz(double *ma, const size_t NFIL, const size_t NCOL) {
	for (size_t i = 0; i < NFIL; i++) {
		for (size_t j = 0; j < NCOL; j++) {
			ma[i * NCOL + j] = i * NCOL + j + 1;
		}
	}
}

void imprimir_matriz(double *ma, const size_t NFIL, const size_t NCOL) {
	for (size_t i = 0; i < NFIL; i++) {
		for (size_t j = 0; j < NCOL; j++) {
			printf("%.02f ", ma[i * NCOL + j]);
		}
		printf("\n");
	}
}

void multiplicar_matrices(double *ma, double *mb, const size_t NFIL, const size_t NCOL, double *res) {
	for (size_t i = 0; i < NFIL; i++) {
		for (size_t j = 0; j < NCOL; j++) {
			res[i * NCOL + j] = 0;
			for (size_t k = 0; k < NCOL; k++) {
				res[i * NCOL + j] += ma[i * NCOL + k] * mb[k * NCOL + j];
			}
		}
	}
}

int main() {
	const size_t NFIL = 4;
	const size_t NCOL = 4;
	const size_t MSIZE = NFIL * NCOL;
	double *ma, *mb, *res;
	ma  = malloc(MSIZE * sizeof(double));
	mb  = malloc(MSIZE * sizeof(double));
	res = malloc(MSIZE * sizeof(double));
	// Inicializar
	inicializar_matriz(ma, NFIL, NCOL);
	inicializar_matriz(mb, NFIL, NCOL);
	// Imprimir
	imprimir_matriz(ma, NFIL, NCOL);
	// Calcular res = ma x mb
	multiplicar_matrices(ma, mb, NFIL, NCOL, res);
	// Imprimir resultado
	printf("\n");
	imprimir_matriz(res, NFIL, NCOL);

	return EXIT_SUCCESS;
}

