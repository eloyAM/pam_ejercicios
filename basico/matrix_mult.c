#include <stdio.h>
#include <sys/types.h>

#define NFIL 4
#define NCOL 4
#define MSIZE NFIL*NCOL

void inicializar_matriz(int ma[NFIL][NCOL]) {
	for (size_t i = 0; i < NFIL; i++) {
		for (size_t j = 0; j < NCOL; j++) {
			ma[i][j] = i * NCOL + j + 1;
		}
	}
}

void imprimir_matriz(int ma[NFIL][NCOL]) {
	for (size_t i = 0; i < NFIL; i++) {
		for (size_t j = 0; j < NCOL; j++) {
			printf("%d ", ma[i][j]);
		}
		printf("\n");
	}
}

void multiplicar_matrices(int ma[NFIL][NCOL], int mb[NFIL][NCOL], int res[NFIL][NCOL]) {
	for (size_t i = 0; i < NFIL; i++) {
		for (size_t j = 0; j < NCOL; j++) {
			res[i][j] = 0;
			for (size_t k = 0; k < NCOL; k++) {
				res[i][j] += ma[i][k] * mb[k][j];
			}
		}
	}
}

int main() {
	int ma[NFIL][NCOL], mb[NFIL][NCOL], res[NFIL][NCOL];
	// Inicializar
	inicializar_matriz(ma);
	inicializar_matriz(mb);
	// Imprimir
	imprimir_matriz(ma);
	// Calcular res = ma x mb
	multiplicar_matrices(ma, mb, res);
	// Imprimir resultado
	printf("\n");
	imprimir_matriz(res);
}

